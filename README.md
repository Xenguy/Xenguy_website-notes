# Xenguy_website-notes

*Xenguy's very unofficial web site notes.*

<!-- markdown-toc start - Don't edit this section. Run M-x markdown-toc-refresh-toc -->
**Table of Contents**

- [Xenguy_website-notes](#xenguy_website-notes)
  - [Coordinates](#coordinates)
    - [Other files](#other-files)
  - [Bittorrent](#bittorrent)
    - [Torrent files](#torrent-files)
    - [Magnet links](#magnet-links)
      - [Update these pages](#update-these-pages)
  - [Documentation](#documentation)
  - [Git](#git)
    - [Clients](#clients)
    - [Help Mr. Wizard!](#help-mr-wizard)
    - [Searching git history](#searching-git-history)
  - [Indentation](#indentation)
  - [Licensing](#licensing)
    - [The Devuan web site](#the-devuan-web-site)
    - [404 page](#404-page)
  - [Linkcheckers](#linkcheckers)
    - [1. linkchecker](#1-linkchecker)
    - [2. W3C Linkcheck(?)](#2-w3c-linkcheck)
    - [3. Tested, but dropped](#3-tested-but-dropped)
  - [Miscellaneous](#miscellaneous)
  - [Mirrors](#mirrors)
    - [Standard pattern for testing a new mirror:](#standard-pattern-for-testing-a-new-mirror)
    - [Resources](#resources)
  - [Sitemap](#sitemap)
  - [Style Guide](#style-guide)
    - [Closing Void Tags](#closing-void-tags)
  - [Upgrades](#upgrades)
    - [Point Releases](#point-releases)
      - [Files that may need to change '3.0' to '3.1'](#files-that-may-need-to-change-30-to-31)
    - [New Releases](#new-releases)
      - [Files to be touched: Beowulf to Chimaera](#files-to-be-touched-beowulf-to-chimaera)
      - [Files to be touched: Chimaera to Daedalus](#files-to-be-touched-chimaera-to-daedalus)
  - [Todo](#todo)

<!-- markdown-toc end -->

## Coordinates

- Source code:
  - [Devuan's git forge](https://git.devuan.org/devuan/www.devuan.org)
  - git clone https://git.devuan.org/devuan/www.devuan.org.git
- Web sites:
  - [Preview server (for development and staging)](https://beta.devuan.org/)
  - [Production server (official web site)](https://devuan.org/)

### Other files

- Banned packages:
  - https://pkgmaster.devuan.org/bannedpackages.txt

## Bittorrent

### Torrent files

No web pages need to be modified when the torrent file changes, as the URL remains the same.

Torrent files can be generated with `mktorrent` on the CLI (needs to be tested), e.g.:
- `mktorrent -v -a udp://tracker0.url,udp://tracker1.url,udp://tracker2.url -o devuan_beowulf.torrent directory_name`

In practice, there is a Makefile on files.d.o that will generate a new torrent file.  For magnet file generation, see section below.

Note that a new torrent file needs to be fully seeded by at least one party before leechers can then download the files, and contribute bandwidth to their distribution.  A standardized method to do this still needs to be worked out.  One possible approach would be to deploy `rtorrent`, which requires only a command-line, and script an initial new torrent seeding solution on one of our servers.  Deltomix, bb|hcb, and onefang have shown some interest in these ideas.

### Magnet links

A magnet link is generated (via a hash?) based on the corresponding torrent file.  Whenever a new torrent file is created, a new magnet link will need to be generated, and also posted to the web site (see below).

Most bittorrent (BT) clients can create a magnet file via their GUI (e.g. qbittorrent).  For those who like command-line (CLI) methods, the `transmission-show` command (part of the `transmission-cli` package) will output a magnet URL to standard out, e.g.:
- `transmission-show -m devuan_beowulf.torrent`

bb|hcb has suggested that a couple of Devuan's mirrors, included in the magnet link (e.g. see 'HTTP sources' tab in the qbittorrent client), might be useful to facilitate faster initial seeding of the torrent. Sample mirror URLs are:
- https://sledjhamr.org/devuan-cd/devuan_daedalus/
- https://mirror.leaseweb.com/devuan/devuan_daedalus/

#### Update these pages

When magnet strings are updated, edit these files accordingly:
- get-devuan.html
- os/announce/beowulf-stable-announce-060120.html (?)

## Documentation

### Documentation Restructuring: Notes of #devuan-www comments from golinux and rrq

#### golinux
- I think that anything not current should be archived in folders perhaps by
  ristributions/releases. That way only current stuff would be at the top of the
  stack
- And dev1fanboys stuff needs to be checked that it is all still current.
- I don't think it hurts to have current documentation available in the documentation area of git. But ATM it is really messy

#### Markdown: to ditch, or to keep?

- Ditch: X
- Keep:  G
- ?:     R; FSR;

- Translations: ditch for now; insufficient resources and value proposition...

#### To-do
- Mount *which* file for Buddy to hack on?
  - Current most update file is on web site AFAICT...
    - Could copy web site version over to 'documentation', to edit in HTML format?
  - Inventory needed of 'documentation' before any files start to move ...
- 'documentation' housecleaning needed; list what needs doing?

rrq's Devuan ISO installer documentation:
https://git.devuan.org/devuan/installer-iso/src/branch/master/docs


## Git

### Clients
- Spacemacs/Magit
  - Fantastic, the best!
- Tig
  - Ncurses git repo browser, and a bit more; decent, mostly complete, while Magit is **fully** complete.

### Help Mr. Wizard!

**Delete all local commits back to a specific commit:**
- git reset --hard \<commit hash\>

**Warning:**  The following are non-reversable actions that modify remote repos (i.e. delete commits that are now pushed to a remote repo).  If others have cloned the repo, they will likely be pissed off that you caused them problems by doing this : -)

**Delete the last remote commit:**
- `git push origin +dd61ab32^:master` <-- where that is the top commit to remove.
- Or: `git reset HEAD^ --hard` THEN `git push origin -f``

**Delete the second last remote commit:**
- `git rebase -i dd61ab32^` <-- where this is the bad commit.
- Text editor opens; delete the line of the above commit, and save/close the file.
- `git push origin -f`  <-- Deletes second last commit from remote origin/master repo.
  
### Searching git history

**Search git history for the instance of a string:**
- `git log -i Sfoo`

**Instead, to simply grep all files in a git repo for a string:**
- `git grep 'sitemap' -- "*.html"`
  
## Indentation

The utility 'tidy' is able to indent HTML code, but I have so far not found a way to ensure that it does **only** that.  For example, when I used the following command-line (CLI) invocation to indent all HTML files on the web site, Tidy also removed the instances of empty `<span>` tags on all the pages, which it had no business doing whatsoever:

- `for i in $(find . -iname "*html"); do tidy --show-errors 0 --show-body-only auto -qim -w 0 $i; done`
- This recursively re-indents all HTML files in place.  Obviously it requires the 'tidy' utility to be installed.

I used the following steps to workaround Tidy's bad behavior:

1. Verify whether there are any current files containing string `>&nbsp;</span>`:
- `for i in $(find . -iname "*.html"); do grep -il '>&nbsp;</span' $i; done | wc -l`
- Expected result is zero such instances found.

2. Count the number of web site files currently containing string `></span>`:
- `for i in $(find . -iname "*.html"); do grep -il '></span' $i; done | wc -l`
- Expected result is (currently) 86 files, i.e. all web site HTML files.

3. Open all web site HTML files with the Vim text editor:
- `find . -iname "*html" -exec vim -c "set nobackup" {} \+`
- Issue the following command in Vim to insert, in all files, the string `&nbsp;` inside all currently empty `<span>` tags:
  - `:bufdo %s!></span>!>\&nbsp;</span>!g`
- Save all files and exit Vim:
  - `:wa`
  - `:qa`

4. Now that the `<span>` tags are no longer empty, Tidy can be safely invoked to reindent the HTML code of all web site HTML files:
- `for i in $(find . -iname "*html"); do tidy --show-errors 0 --show-body-only auto -qim -w 0 $i; done`

5. Open all web site HTML files again with the Vim text editor:
- `find . -iname "*html" -exec vim -c "set nobackup" {} \+`
- Issue the following command in Vim to remove, in all HTML files, the string `&nbsp;` currently inside some `<span>` tags:
  - `:bufdo %s!>&nbsp;</span>!></span>!g`
- Save all files and exit Vim:
  - `:wa`
  - `:qa`
  
6. Finally, verify that all strings `>&nbsp;</span` have been removed from all site HTML files:
- `for i in $(find . -iname "*.html"); do grep -il '>&nbsp;</span' $i; done | wc -l`
- Expected result is zero such instances found.

For indenting the file you are currently editing:

- If you use the Vim editor:
  - `:%!tidy --show-errors 0 --show-body-only auto -qi -w 0`
  - But remember the issue above with Tidy deleting empty `<span>` tags  : -)


## Licensing

### The Devuan web site

[TBC...]

### 404 page

- i.e. https://devuan.org/404.html


## Linkcheckers

**Conclusion:**  Linkchecker wins, unless proven to provide wrong or incomplete results in future.  Also it can be supplemented/supported by use of the W3C online link checking tool.


### 1. linkchecker

- Seems very fast (e.g. checks the whole site in just under 2 minutes)
- Easy to use, at the command-line.
- Has a GUI named 'linkchecker-gui' (probably won't try this, so can't report back)

Edit:  linkchecker turns out to be harder to use than expected, e.g.:
- So far unable to wrangle linkchecker to check just one single page.
- Example command line invocations:
  - linkchecker -v -r1 --check-extern --ignore-url=/gitlab-issues https://beta.devuan.org/get-devuan.html | tee /tmp/linkchecker.txt
    - Was trying to check only this single page, but linkchecker checks the whole site as far as I can tell.
- So, this is a work in progress, and I am wondering about perhaps choosing the W3C online link checking tool as the default method...

### 2. W3C Link Checker


- [Free online linkchecking service](https://validator.w3.org/checklink).
- Need to specify recursion depth (e.g. '7' levels)
- Checks more than just links.
  - e.g. Checks for duplicate 'id="foo"'
- Very verbose output; haven't tried checking the 'Summary' option...
- Takes about 10 minutes to run the site (so, slower than 'linkchecker' above [2 minutes vs. 10 minutes])

  
### 3. Tested, but dropped

- linklint
- checkbot
- webcheck
  - The most promising of this lot, but didn't seem to want to process our web site, for reasons unknown.

## Miscellaneous

### Happy New Years

#### www.devuan.org
- Need to update the copyright dates in the footer of all HTML pages (e.g. `bufdo :%s!2014-2024!2014-2025!`)
- To invoke all web site HTML files for editing in Vim:
  - `find . -iname "*html" -exec vim -c "set nobackup" {} \+`

#### Other locations
- archive.d.o and pkgmaster.d.o do not have headers/footers, and therefore need no such edits.

- pkginfo.d.o: (rrq?)
  - https://git.devuan.org/devuan/tellpackage/src/commit/242d0bcf0b53bc66c5b0e7172961a447caeff172/templates/common.xsl#L84
    - Is some deployment process required to initialize a change to the
    above file?

- files.devuan.org: (fsmithred?)
  - files:/home/files.devuan.org/public/.footer.html
  - files:/home/files.devuan.org/embedded/.footer.html

## Mirrors

### Resources

Note that according to rrq, "package mirrors and installation ISO mirrors are different collections".
- Instructions to set up a package mirror:  https://pkgmaster.devuan.org/devuan_mirror_walkthrough.txt
- Instructions to set up an ISO mirror:  https://files.devuan.org/MIRRORS.txt

When adding or deleting ISO mirrors, edit this page: https://devuan.org/get-devuan .

One canonical list of package mirrors is located here:  https://pkgmaster.devuan.org/mirror_list.txt

This page monitors the health of Devuan's package mirrors:
- https://sledjhamr.org/apt-panopticon/results/Report-web.html or
- http://veritas.devuan.org/apt-panopticon/results/Report-web.html

bb|hdb states (2024-04-28):
- A full mirror is 215GB (that includes torrent seed, meaning that .iso are there twice)
- 61G torrents/
- 24G mirror/devuan
- 131G mirror/devuan-cd/
- 215G total
- You can safely skip the torrents part.

### Standard pattern for testing a new mirror:

1. `links https://repo.ifca.es/devuan-cd/`
2. `links http://repo.ifca.es/devuan-cd/`
3. `links ftp://repo.ifca.es/devuan-cd/`
4. `rsync -avzPh repo.ifca.es::devuan-cd/devuan_ascii .`

One can check whether a domain uses an IPv6 address with 'dig':
- `dig repo.ifca.es AAAA`

but trying an IPv6 address/URL does not work on my network currently:
- `elinks http://[2001:720:1c14::ffff:c192:4b5d]/`

so check with onefang, who seems able to verify IPv6 functionality.


## Style Guide

### Closing Void Tags

In short, HTML5 will accept either, for example, `<br>` or `<br />`.  The most important point is probably to just pick one, and then be consistent. The site already uses mainly `<br>`, so we will continue to use that form, and the same for the `<img>` tag, etc.

## Sitemap

The file `https://www.devuan.org/sitemap.xml` is currently generated by a shell-script called `gen-sitexml.sh`.

It is regenerated each time new content is published to the Live site.

It's purpose is to interact with indexing bots in conjunction with the `robots.txt` file.

It is updated on the Live site, but not the Dev site.

## Upgrades

### Archived Releases

#### Files to be touched

- [ ] https://beta.devuan.org/index.html
  - See section 'Devuan Releases'.
- [ ] https://www.devuan.org/os/releases.html
  - Edit table.
- [ ] https://www.devuan.org/os/packages.html
  - Edit appropriate section.

### Point Releases

#### Files that need to change '3.0' to '3.1'

- [x] 1. https://beta.devuan.org/os/explore.html
   - Section 'Announcements':  Create '3.1.0 Beowulf Point Release Announcement' link.
     - Links to **new page**:  https://beta.devuan.org/os/announce/beowulf-point-release-announce-021421
       - Above new page based on previous point release announcement:  https://beta.devuan.org/os/announce/ascii-point-release-announce-112119
- [x] 2. https://beta.devuan.org/index.html
   - Section 'Devuan Releases':  s/Beowulf 3.0.0/Beowulf 3.1.0 point release/
     - Links to **new page**:  https://beta.devuan.org/os/announce/beowulf-point-release-announce-021421
- [x] 3. https://beta.devuan.org/os/documentation/index.html
   - Section: Upgrade or Migrate to Beowulf
     - s/Devuan Beowulf 3.0 Release Notes/Devuan Beowulf 3.1.0 Release Notes/
   - Section: Install Beowulf (with screenshots)
     - s/Devuan Beowulf 3.0 Release Notes/Devuan Beowulf 3.1.0 Release Notes/
- [x] 4. https://beta.devuan.org/os/documentation/install-guides/beowulf/install-devuan.html
   - Section: Contents
     - [x] s/Devuan Beowulf 3.0 Release Notes/Devuan Beowulf 3.1.0 Release Notes/
     - [ ] Check for file at existing URL location and delete if necessary
     - [x] Correct the link to the canonical link on files.d.o
- [x] 5. https://beta.devuan.org/os/index.html
   - Section: Beowulf Overview
     - s/Stable version 3.0.0/Stable version 3.1.0/
- [x] 6. https://beta.devuan.org/os/packages.html
   - Section: Default configurations
     - s/Devuan 3.0.0 Beowulf (stable)/Devuan 3.1.0 Beowulf (stable)/
- [x] 7. https://beta.devuan.org/os/filenaming.html
   - Bottom of page:  s/devuan_beowulf_3.0.0_amd64_netinstall.iso/devuan_beowulf_3.1.0_amd64_netinstall.iso/

#### Files to be touched

- [x] Magnet URL to be regenerated once new torrent file is created.
- [x] Test new torrent file and magnet URL.
- [x] New Beowulf point release announce page:
  - [x] Keep or remove ARM 'torrents to come' sentence?
- [x] Need to recover lost commit that updates installation image to include Runit.

#### Lessons learned

- Always link check any new or modified links on above pages before pushing to Live.
- Creating a separate local 'point-release-x.x' branch works well, however, for changes to be reviewed by others on the Dev server, this branch needs to be merged with the main branch first.
- For the new page that needs to be created, work with the usual suspects to revise necessary content on that page.

### New Releases

#### Files to be touched: Beowulf to Chimaera

##### www repo
- https://beta.devuan.org/index.html
  - Q. Should ASCII be archived?
  - [ ] Review: line 79:
    - Plasma41: "It is possible to upgrade from Devuan Beowulf or migrate from
      Debian Bullseye, Stretch or Jessie to Devuan Chimaera." I don't we should
      be indicating that it's a simple migration to go directly from Stretch or
      Jessie to Chimaera. As far as I know, it's not. Buster to Chimaera should
      work.
  - There is an 'announce' document linked to from this page:  /os/announce/chimaera-release-announce-2021-10-04
    - [ ] Q. Where is this document in git?
- https://beta.devuan.org/os/index.html
  - [ ] Review: line 69.  Verify release date.
  - [x] Review: line 74.  Verify kernel version.
  - [x] Review: line 78.  Verify init systems supported.
  - [ ] Note: Section 'Supported Architectures', are these the same as Beowulf?
  - [ ] Note: Section 'Desktop Environments', are these the same as Beowulf?
  - [ ] Note: Section 'Available images', sizes will need to be checked and updated.
- https://beta.devuan.org/os/releases.html
  - [ ] Review: line 75:  is ASCII to be archived?
  - [ ] Review: line 81.
  - [x] Review: line 101.
  - [x] Review: line 103.
- https://beta.devuan.org/os/packages.html
  - See section 'Primary Signing Keys':  still up-to-date?
  - Should 'ASCII' be moved to state 'archived', like Jessie?
  - Should 'Jessie' be removed from web page, or ...?
  - Tor access needs to be updated, and tested?
- https://beta.devuan.org/os/install.html
  - [ ] See: Devuan Beowulf Release Notes detail important changes between the previous and current release.
    - Need to verify that link to Release Notes works (currently it is 404).
  - [x] 'git store' link on last line wants login credentials; should make link more generic.
  - See 'Upgrade or Migrate to Beowulf' section:
    ? Q. Does this need to be cloned to a similar Chimaera section?
      ? A. Yes.
    - [ ] Review: line 83.
    - [ ] Review: line 88.
    - Same question for section 'Install Beowulf (with screenshots)'...
  - [ ] Need to create new document 'Install Chimaera (with screenshots)'
- https://beta.devuan.org/os/explore.html
  - [ ] URL date needs to be finalized:  See '4.0.0 Chimaera Stable Announcement'
  - [ ] Need to create new page '/os/announce/chimaera-stable-announce-2021-08-??'
  - [ ] Section 'Install': Need to create new linked bullet 'Upgrade or Migrate to Chimaera'
    - Links to http://beta.devuan.org/os/install.html#upgrade-or-migrate-to-chimaera
  ? [ ] 'Additional documentation' section needs modification?
- https://beta.devuan.org/os/filenaming.html
  - See last sentence filename reference.
- https://beta.devuan.org/os/documentation/index.html
  - Section 'Upgrade or Migrate to Beowulf': need similar section, for Chimaera.
  - Section 'Install Beowulf (with screenshots)': need similar section, for Chimaera.

Also:
- Any page that references 'torrent' ...
  - https://beta.devuan.org/get-devuan.html
- Update all pages:
  - meta tags: content="debian, jessie, ascii, beowulf,
- https://beta.devuan.org/os/packages.html
  - See:  Devuan recommends using the codename (beowulf, chimaera, daedalus, ceres)

- Devuan derivatives
  - https://beta.devuan.org/os/devuan-distros
    - https://heads.dyne.org/ (still active?)
    - https://decodeos.dyne.org/
      - Still references 'freenode' ...
  - https://beta.devuan.org/os/init-freedom
    - [ ] Section 'Alternate Init Systems': golinux suggests link '66-devuan (no link)' should be removed.


##### Images to update (with new theme color)

- /ui/img/rounded-square.png
- /ui/img/if.png
- /ui/img/devuan-logo.png


##### CSS files to update (with new theme color)


###### Main website CSS file:
- /ui/css/devuan.css 

###### Bug reports, and Popcon (those buggers):
- /ui/css/debbugs/bugs.css
- /ui/css/debbugs/buggers.css

###### Pkginfo:
- /ui/css/pkginfo/file-query.css
- /ui/css/pkginfo/common.css
- /ui/css/pkginfo/debtree-query.css
- /ui/css/pkginfo/package-query.css
- /ui/css/pkginfo/policy-query.css

##### installer-iso repo

.../README.txt
.../README.html
[There are others... TBC... ]

#### Files to be touched: Chimaera to Daedalus

##### www repo

###### '\<\!-- release' method

All known release update points have been tagged with HTML comments, as follows:
1. \<\!-- release
   - During release, usually a sign that that a given item has already been updated.  Otherwise, these HTML comments remain to serve as markers for the next release.
2. \<\!-- release update
   - During release, items that must still be updated.
3. \<\!-- release review
   - During release, items that must still be reviewed (this can imply that consultation with other Devuan team members is needed, to confirm a given condition).

These HTML comments can be easily located by using the following git command-line from the 'source' directory:
- `git grep -in '!-- release' -- "*.html"`

###### Deprecated in favor of: '\<\!-- release' method (see above)

- https://beta.devuan.org/index.html
  - [ ] Finalize link for release 'announce' file: line 61:
  ? There is an 'announce' document linked to from this page:  /os/announce/chimaera-release-announce-2021-10-04
    - [ ] Q. Where is this document in git?
- https://beta.devuan.org/get-devuan.html
  - [ ] Section 'Torrent and Magnet Link'
- https://beta.devuan.org/os/index.html
  - [ ] Review: Verify release date.
  - [ ] Review: Verify kernel version.
  - [ ] Review: Verify init systems supported.
  - [ ] Review: Section 'Supported Architectures', are these the same as Chimaera?
  - [ ] Review: Section 'Desktop Environments', are these the same as Chimaera?
  - [ ] Review: Section 'Available images', sizes will need to be checked and updated.
  - [ ] Review: Update version name of last line of content.
- https://beta.devuan.org/os/explore.html
  - [ ] URL date needs to be finalized:  See '5.0.0 Daedalus Stable Announcement'
  - [ ] Need to create new page '/os/announce/chimaera-stable-announce-2021-08-??'
  - [ ] Section 'Install': Need to create new linked bullet 'Upgrade or Migrate to Chimaera'
    - Links to http://beta.devuan.org/os/install.html#upgrade-or-migrate-to-chimaera
  ? [ ] 'Additional documentation' section needs modification?
- https://beta.devuan.org/os/releases.html
  - [ ] Review: line 81.
  - [ ] Review: line 101.
  - [ ] Review: line 103.
- https://beta.devuan.org/os/packages.html
  - See section 'Primary Signing Keys':  still up-to-date?
  - Should 'Jessie' be removed from web page, or ...?
  - Tor access needs to be updated, and tested?
- https://beta.devuan.org/os/install.html
  - [ ] See: Devuan Chimaera Release Notes detail important changes between the previous and current release.
    - Need to verify that link to Release Notes works.
  - See 'Upgrade or Migrate to Chimaera' section:
    ? Q. Does this need to be cloned to a similar Chimaera section?
      ? A. Yes.
    - [ ] Review: line 83.
    - [ ] Review: line 88.
    - Same question for section 'Install Chimaera (with screenshots)'...
  - [ ] Need to create new document 'Install Daedalus (with screenshots)?'
- https://beta.devuan.org/os/filenaming.html
  - [ ] Update last sentence filename reference.
- https://beta.devuan.org/os/documentation/index.html
  - Section 'Upgrade or Migrate to Beowulf': need similar section, for Chimaera.
  - Section 'Install Beowulf (with screenshots)': need similar section, for Chimaera.

###### Also...

- Any page that references 'torrent' ...
  - https://beta.devuan.org/get-devuan.html
[x] Update all pages:
  [x] meta tags: content="debian, jessie, ascii, beowulf,
- https://beta.devuan.org/os/packages.html
  - See:  Devuan recommends using the codename (beowulf, chimaera, daedalus, ceres)

- Devuan derivatives
  - https://beta.devuan.org/os/devuan-distros
    - https://heads.dyne.org/ (still active?)
    - https://decodeos.dyne.org/
      - Still references 'freenode' ...
  - https://beta.devuan.org/os/init-freedom
    - [ ] Section 'Alternate Init Systems': golinux suggests link '66-devuan (no link)' should be removed.


##### documentation repo

[...]

##### Images to update (with new theme color)

- /ui/img/rounded-square.png
- /ui/img/if.png
- /ui/img/devuan-logo.png

##### CSS files to update (with new theme color)

###### Main website CSS file:
- /ui/css/devuan.css 

###### Bug reports, and Popcon (those buggers):
- /ui/css/debbugs/bugs.css
- /ui/css/debbugs/buggers.css

###### Pkginfo:
- /ui/css/pkginfo/file-query.css
- /ui/css/pkginfo/common.css
- /ui/css/pkginfo/debtree-query.css
- /ui/css/pkginfo/package-query.css
- /ui/css/pkginfo/policy-query.css

##### installer-iso repo

.../README.txt
.../README.html
[There are others... TBC... ]

## Todo

<!-- Older stuff -->
- Need to copy a template file for Chimaera Release Announcement (https://www.devuan.org/os/announce/beowulf-stable-announce-060120) to 'release-docs' directory in 'documentation' repo.
- New Chimaera SVG image emailed from golinux...
- https://www.devuan.org/os/announce/beowulf-point-release-announce-021421
  - Revert the recent edit, 'freenode->libera.chat' after Chimaera is officially
    released. Perhaps that might suffice as a compromise plan? 23:51 plasma41
    That would suffice.
- Arm: (fsr)
  - Link arm-files.devuan.org from the download page.
- Magnet file temporarily commented out.  Need to:
  - Find a more reliable method of generating new magnet URLs.
  - Test in real time to confirm magnet URLs are really working to connect to speedy torrents.
- buster-to-beowulf documentation needs a disclaimer added:
  - Documentation no longer works seamlessly...
  - If anyone finds a seamless install method, please share with others...
- https://www.devuan.org/os/packages, add sources.list example: (bgstack15)
  - deb http://deb.devuan.org/merged ceres main non-free contrib
- Finalize linkchecking and repair of broken links
  - Chat with fsmithred about a couple of those use cases (involving 'uboot' ?).
- ~~get-devuan.html:  find out if rsync links are syntactically correct (e.g. '/' vs. '::')~~
  - ~~Find a simple way to test RSYNC mirrors.~~
- ~~Cosmetic changes (mostly just 2-space indentation of the HTML code), all in one pass.~~:
